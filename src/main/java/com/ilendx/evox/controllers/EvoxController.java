package com.ilendx.evox.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.ilendx.evox.service.EvoxService;

/**
 * EvoxController
 *
 */
@RestController
@RequestMapping("/evox")
public class EvoxController {
	
	 @Autowired
	 private EvoxService evoxService;
	 
	 /**
	 * Call EvoxService to get Vehicle Data
	 * @param year year
	 * @param make make
	 * @param model model
	 * @return Vehicle data json string returned from evox service
	 */
	@RequestMapping(value = "/vehicleData/{year}/{make}/{model}", method = RequestMethod.GET, produces = "application/json")
	 public String getVehicleData(@PathVariable(value = "year") String year,@PathVariable(value = "make") String make,@PathVariable(value = "model") String model ) {
		 return evoxService.getVehicleData(year, make, model);
	 }
	
	 /**
	 * Call EvoxService to get Vehicle Product Data 
	 * @param vifNum
	 * @return Vehicle Product Data json string returned from evox service
	 */
	@RequestMapping(value = "/vehicleProductData/{vifNum}", method = RequestMethod.GET, produces = "application/json")
	 public String getVehicleProductData(@PathVariable(value = "vifNum") String vifNum ) {
		 return evoxService.getVehicleProductData(vifNum);
	 }
}
