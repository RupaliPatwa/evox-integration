package com.ilendx.evox;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import ru.vyukov.prometheus.starter.EnablePrometheus;

/**
 * Launch point for Spring Boot application
 */
@SpringBootApplication
@EnablePrometheus
public class Application {

	public static void main(String[] args) {
		SpringApplication.run(Application.class, args);
	}

}
