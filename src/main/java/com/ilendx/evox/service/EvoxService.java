package com.ilendx.evox.service;

/**
 * EvoxService
 * Calls Evox image service from public methods
 */
public interface EvoxService {
	
	/**
	 * @param year Vehicle Year
	 * @param make Vehicle Make
	 * @param model Vehicle Model
	 * @return Vehicle Data json string by call evox images service
	 */
	public String getVehicleData(String year, String make, String model);
	/**
	 * @param vifNum
	 * @return Vehicle Product Data json string by call evox images service
	 */
	public String getVehicleProductData(String vifNum);

}
