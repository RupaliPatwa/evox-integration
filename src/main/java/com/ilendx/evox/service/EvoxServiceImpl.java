package com.ilendx.evox.service;

import java.util.HashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixProperty;

/**
 * EvoxServiceImpl
 *
 */
@Service
public class EvoxServiceImpl implements EvoxService{
	
	private static final Logger log = LoggerFactory.getLogger(EvoxServiceImpl.class);

	private final static String SETTING_URL = "http://bank-configuration/bankConfig/setting/{settingKey}";
	private static final String SERVICE_TIMEOUT = "60000";
	private static final String EVOX_VEHICLE_DATA_URL = "http://api.evoximages.com/api/v1/vehicles/";
	private static final String EVOX_VEHICLE_PRODUCT_URL = "http://api.evoximages.com/api/v1/vehicles/{vifnum}/products/{productId}/{productTypeId}";

	private final RestTemplate restTemplate;
	
	public EvoxServiceImpl(RestTemplateBuilder restTemplateBuilder) {
		this.restTemplate = restTemplateBuilder.build();
	}
	
	
	/* (non-Javadoc)
	 * @see com.ilendx.evox.service.EvoxService#getVehicleData(java.lang.String, java.lang.String, java.lang.String)
	 */
	@Override
	@HystrixCommand(commandKey = "GetVehicleDataFromEvox", fallbackMethod = "fallbackVehicleData", commandProperties = {
			@HystrixProperty(name = "execution.isolation.thread.timeoutInMilliseconds", value = SERVICE_TIMEOUT) })
	public String getVehicleData(String year, String make, String model) {
		
		//get evox-api-key's value from bank-config/bank-setting
		String evoxApiKey = getEvoxAPIKey();
		HttpHeaders headers = new HttpHeaders();
    	headers.add("x-api-key", evoxApiKey);
    	
    	UriComponentsBuilder uriBuilder = UriComponentsBuilder.fromHttpUrl(EVOX_VEHICLE_DATA_URL)
    			.queryParam("year", year)
				.queryParam("make", make)
				.queryParam("model", model);
				
    	HttpEntity<String> httpEntity = new HttpEntity<String>(headers);

    	ResponseEntity<String> response = restTemplate.exchange(uriBuilder.build().encode().toUri(),HttpMethod.GET,httpEntity,String.class);
    	return response.getBody();
	}
	
	/* (non-Javadoc)
	 * @see com.ilendx.evox.service.EvoxService#getVehicleProductData(java.lang.String)
	 */
	@Override
	@HystrixCommand(commandKey = "GetVehicleProductDataFromEvox", fallbackMethod = "fallbackVehicleProductData", commandProperties = {
			@HystrixProperty(name = "execution.isolation.thread.timeoutInMilliseconds", value = SERVICE_TIMEOUT) })
	public String getVehicleProductData(String vifNum) {
		
		//get evox-api-key's value from bank-config/bank-setting
		String evoxApiKey = getEvoxAPIKey();
		
		HttpHeaders headers = new HttpHeaders();
    	headers.add("x-api-key", evoxApiKey);
    	HttpEntity<String> requestEntity = new HttpEntity<String>(headers);
    	
    	Map<String,String> params = new HashMap<String, String>();
    	params.put("vifnum", vifNum);
    	params.put("productId", "2");
    	params.put("productTypeId", "41");
    	
    	ResponseEntity<String> response  = restTemplate.exchange(EVOX_VEHICLE_PRODUCT_URL,HttpMethod.GET,requestEntity,String.class,params);
    	return response.getBody();
	}
	
	/**
	 * Called if evox call to get vehicle data times out
	 * @param vin The vehicle's VIN
	 * @return the fallback to the vehicle data
	 */
	public String fallbackVehicleData(String vin) {
		log.info("Unable to contact evox  for vehicle data. Returning fallback.");
		return "";
	}
	
	/**
	 * Called if evox call to get vehicle product data times out
	 * @param vin The vehicle's VIN
	 * @return the fallback to the vehicle data
	 */
	public String fallbackVehicleProductData(String vin) {
		log.info("Unable to contact evox for vehicle product data. Returning fallback.");
		return "";
	}
	
	/**Get evox-api-key's value from bank-config/bank_setting table
	 * 
	 * @return evox-api-key value
	 */
	private String getEvoxAPIKey() {
		
		return getBankConfigurationSetting("evox-api-key");
	}
	
	/**
	 * Get settingKey's value from bank-config/bank setting
	 * 
	 * @param setting settingKey
	 * @return settingKey's value from bank-config/bank setting
	 */
	@HystrixCommand(commandKey = "getSettingFromBankConfiguration", commandProperties = {
			@HystrixProperty(name = "execution.isolation.thread.timeoutInMilliseconds", value = SERVICE_TIMEOUT) })
	private String getBankConfigurationSetting(String setting) {
		try {
				String json = restTemplate.getForObject(SETTING_URL, String.class, setting);
				ObjectMapper mapper = new ObjectMapper();
				Map<String, Object> map = mapper.readValue(json, new TypeReference<Map<String, Object>>() {
				});
				Object value = map.get("value");
				if (value != null) {
					return value.toString();
				} else {
					return null;
				}
		} catch (Exception e) {
			throw new RuntimeException(e.getMessage());
		}
	}
	


}
